The GPAW calculator object
==========================
.. module:: gpaw.calculator
.. module:: gpaw

.. autoclass:: GPAW
   :members:
   :inherited-members:
